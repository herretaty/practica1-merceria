package com.tatiana.merceria.base;

import java.time.LocalDate;

public class Pijamas extends Merceria {
    public int talla;

    public Pijamas(){
        super();
    }

    public Pijamas(String nombre, String marca, String tipo, LocalDate fechaRegistro, int talla ){
       super(nombre,marca,tipo,fechaRegistro);
       this.talla=talla;
    }

    public int getTalla() {
        return talla;
    }

    public void setTalla(int talla) {
        this.talla = talla;
    }

    @Override
    public String toString() {
        return "Pijamas:"+"\n Nombre Prenda: "+getNombre()+"\n Marca:"+getMarca()+
                "\n Tipo:"+getTipo()+"\n Talla:"+getTalla();
    }
}
