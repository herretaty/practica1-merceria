package com.tatiana.merceria.base;

import java.time.LocalDate;

public abstract class Merceria {
    private String nombre;
    private String marca;
    private String tipo;
    private LocalDate fechaRegistro;

    public Merceria(String nombre, String marca, String tipo, LocalDate fechaRegistro){
        this.nombre= nombre;
        this.marca= marca;
        this.tipo= tipo;
        this.fechaRegistro= fechaRegistro;
    }

    public Merceria(){

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public LocalDate getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDate fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
}


