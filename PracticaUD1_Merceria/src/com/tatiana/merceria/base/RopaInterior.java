package com.tatiana.merceria.base;

import java.time.LocalDate;

public class RopaInterior extends Merceria {
    public double precio;

    public RopaInterior(){
        super();
    }
    public RopaInterior(String nombre, String marca, String tipo, LocalDate fechaRegistro, double precio){
        super(nombre, marca, tipo, fechaRegistro);
        this.precio= precio;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "RopaInterior:"+"\n Nombre Prenda: "+getNombre()+"\n Marca:"+getMarca()+
                "\n Tipo:"+getTipo()+"\n Precio:"+getPrecio();
    }
}
