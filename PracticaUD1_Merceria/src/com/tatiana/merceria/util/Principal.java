package com.tatiana.merceria.util;

import com.tatiana.merceria.gui.Controlador;
import com.tatiana.merceria.gui.Modelo;
import com.tatiana.merceria.gui.Vista;

public class Principal {

    public static void main(String[] args) {
        Vista vista= new Vista();
        Modelo modelo= new Modelo();
        Controlador controlador= new Controlador(vista,modelo);
    }
}
