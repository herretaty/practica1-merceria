package com.tatiana.merceria.gui;

import com.tatiana.merceria.base.Merceria;
import com.tatiana.merceria.base.Pijamas;
import com.tatiana.merceria.base.RopaInterior;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * @author tatianaHerrera
 */

/**
 * Clase modelo
 */
public class Modelo {
    private ArrayList<Merceria>listaMerceria;


    public Modelo(){
        listaMerceria= new ArrayList<Merceria>();
    }

    public ArrayList<Merceria> obtenerMerceria(){
        return listaMerceria;
    }

    /**
     * Este metedo es el encargado de almacenar todos nuestros atributos que le hemos puesto
     * y añadirlos en la arrayList
     * @param nombre
     * @param marca
     * @param tipo
     * @param fechaRegistro
     * @param talla
     */
    public void altaPijama(String nombre, String marca, String tipo, LocalDate fechaRegistro, int talla) {
        Pijamas nuevoPijama= new Pijamas(nombre,marca,tipo,fechaRegistro,talla);
        listaMerceria.add(nuevoPijama);

    }

    /**
     *
     * @param nombre
     * @param marca
     * @param tipo
     * @param fechaRegistro
     * @param precio
     */

    public void altaropaInterior(String nombre, String marca, String tipo, LocalDate fechaRegistro, double precio) {
        RopaInterior nuevaRopaInterior= new RopaInterior(nombre, marca, tipo, fechaRegistro,precio);
        listaMerceria.add(nuevaRopaInterior);
    }

    /**
     * En este metodo, nos permitira importar, el archivo exportado desde nuestra aplicacion,
     * enseñandonos los datos que hemos añadido
     * @param fichero
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */

    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
     listaMerceria= new ArrayList<Merceria>();
     Pijamas nuevoPijama= null;
     RopaInterior nuevaRopaInterior= null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i= 0; i<listaElementos.getLength();i++){
            Element nodoMerceria= (Element)listaElementos.item(i);

            if (nodoMerceria.getTagName().equals("Pijama")){
                nuevoPijama= new Pijamas();
                nuevoPijama.setNombre(nodoMerceria.getChildNodes().item(0).getTextContent());
                nuevoPijama.setMarca(nodoMerceria.getChildNodes().item(1).getTextContent());
                nuevoPijama.setTipo(nodoMerceria.getChildNodes().item(2).getTextContent());
                nuevoPijama.setFechaRegistro(LocalDate.parse(nodoMerceria.getChildNodes().item(3).getTextContent()));
                nuevoPijama.setTalla(Integer.parseInt(nodoMerceria.getChildNodes().item(4).getTextContent()));
                listaMerceria.add(nuevoPijama);

            }else if (nodoMerceria.getTagName().equals("Ropa Intertior")){
                nuevaRopaInterior= new RopaInterior();
                nuevaRopaInterior.setNombre(nodoMerceria.getChildNodes().item(0).getTextContent());
                nuevaRopaInterior.setMarca(nodoMerceria.getChildNodes().item(1).getTextContent());
                nuevaRopaInterior.setTipo(nodoMerceria.getChildNodes().item(2).getTextContent());
                nuevaRopaInterior.setFechaRegistro(LocalDate.parse(nodoMerceria.getChildNodes().item(3).getTextContent()));
                nuevaRopaInterior.setPrecio(Double.parseDouble(nodoMerceria.getChildNodes().item(4).getTextContent()));
                listaMerceria.add(nuevaRopaInterior);
            }

        }
    }

    /**
     * En este metodo nos exportara un fichero xml que nosotros guardaremos en nuestro equipo
     * @param fichero
     * @throws ParserConfigurationException
     * @throws TransformerException
     */

    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        Element raiz = documento.createElement("Pijamas");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoMerceria = null, nodoDatos = null;
        Text texto = null;

        for (Merceria unaMerceria: listaMerceria){
            if (unaMerceria instanceof Pijamas){
                nodoMerceria= documento.createElement("Pijama");
            } else {
                nodoMerceria= documento.createElement("RopaInterior");
            }
            raiz.appendChild(nodoMerceria);

            nodoDatos = documento.createElement("nombre");
            nodoMerceria.appendChild(nodoDatos);

            texto= documento.createTextNode(unaMerceria.getNombre());
            nodoDatos.appendChild(texto);

            nodoDatos= documento.createElement("marca");
            nodoMerceria.appendChild(nodoDatos);

            texto= documento.createTextNode(unaMerceria.getMarca());
            nodoDatos.appendChild(texto);

            nodoDatos= documento.createElement("tipo");
            nodoMerceria.appendChild(nodoDatos);

            texto=documento.createTextNode(unaMerceria.getTipo());
            nodoDatos.appendChild(texto);

            nodoDatos= documento.createElement("Fecha-registro");
            nodoMerceria.appendChild(nodoDatos);

            texto= documento.createTextNode(unaMerceria.getFechaRegistro().toString());
            nodoDatos.appendChild(texto);

            if (unaMerceria instanceof Pijamas){
                nodoDatos= documento.createElement("talla");
                nodoMerceria.appendChild(nodoDatos);
                texto=documento.createTextNode(String.valueOf(((Pijamas) unaMerceria).getTalla()));

            }else {
                nodoDatos= documento.createElement("precio");
                nodoMerceria.appendChild(nodoDatos);
                texto=documento.createTextNode(String.valueOf(((RopaInterior)unaMerceria).getPrecio()));
            }
            nodoDatos.appendChild(texto);

        }
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);

    }

    /**
     * Metedo que elimina la seleccion actual
     * @param seleccion
     */
    public void eliminar(Merceria seleccion) {
        listaMerceria.remove(seleccion);
    }

    /**
     * Metodo que busca en la lista
     * @param marca
     * @return
     */

    public Merceria buscar(String marca){
        for (Merceria merceria: listaMerceria){
            if (marca.equalsIgnoreCase(merceria.getMarca()));
            return merceria;
        }
        return null;
    }

    /**
     * Metodo que te avisa que el nombre metido ya existe
     * @param nombre
     * @return
     */

    public boolean existeNombre(String nombre) {
        for (Merceria unaMerceria: listaMerceria){
            if (unaMerceria.getNombre().equals(nombre)){
                return true;
            }
        }
        return false;
    }

}
