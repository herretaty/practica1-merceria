package com.tatiana.merceria.gui;

import com.tatiana.merceria.base.Merceria;
import com.tatiana.merceria.base.Pijamas;
import com.tatiana.merceria.base.RopaInterior;
import com.tatiana.merceria.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
/**
 * @autor tatianaHerrera
 */

/**
 * clase controlador, donde recibimos los inputs desde la vista
 * y tambien es la clase que se comunica con la clase modelo
 */

public class Controlador implements ActionListener, ListSelectionListener, WindowListener {
    private Vista vista;
    private Modelo modelo;
    private File ultimaRutaExportada;

    /**
     * constructor
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.modelo= modelo;
        this.vista= vista;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);

    }

    /**
     * Metodo donde llamaremos a nuestros botones
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionComannd= e.getActionCommand();
        switch (actionComannd){
            case"NUEVO":
                if (hayCamposVacios()){
                    Util.mensajeError("Los siguientes campos no pueden estar vacios\n"+
                         "Nombre\nMarca\nTipo\nFecha"+ vista.precioTalla.getText());
                    break;
                }
                if (modelo.existeNombre(vista.nombretxt.getText())){
                    Util.mensajeError("Ya existe un complemento con ese nombre\n+" +
                            vista.nombretxt.getText());
                    break;
                }
                if (vista.PIJAMASRadioButton.isSelected()){
                    modelo.altaPijama(vista.nombretxt.getText(),vista.marcatxt.getText(),vista.tipotxt.getText(),
                            vista.fechaRegistro.getDate(),Integer.parseInt(vista.telatxt.getText()));
                }else {
                    modelo.altaropaInterior(vista.nombretxt.getText(),vista.marcatxt.getText(),vista.tipotxt.getText(),
                            vista.fechaRegistro.getDate(),Integer.parseInt(vista.telatxt.getText()));
                }
                limpiarCampos();
                refrescar();
                break;
            case "ELIMINAR":
                if (JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el elemento seleccionado?", "Eliminar",
                        JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                    return;
                Merceria seleccionada = (Merceria) vista.list1.getSelectedValue();
                modelo.eliminar((Merceria) seleccionada);
                refrescar();
                break;
            case "BUSCAR MARCA":
                vista.dlmMercercia.clear();
                vista.dlmMercercia.addElement(modelo.buscar(vista.BUSCARtxt.getText()));
                break;

            case "IMPORTAR":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case"EXPORTAR":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case"PIJAMAS":
                vista.precioTalla.setText("TALLA");
                break;
            case"ROPA INTERIOR":
                vista.precioTalla.setText("PRECIO");
                break;

        }

    }

    private boolean hayCamposVacios() {
        if (vista.telatxt.getText().isEmpty()||
                vista.nombretxt.getText().isEmpty() ||
                vista.marcatxt.getText().isEmpty() ||
                vista.tipotxt.getText().isEmpty()||
                vista.fechaRegistro.getText().isEmpty()){
            return true;
        }
        return false;
    }
    private void limpiarCampos() {
        vista.telatxt.setText(null);
        vista.nombretxt.setText(null);
        vista.marcatxt.setText(null);
        vista.tipotxt.setText(null);
        vista.fechaRegistro.setText(null);
        vista.nombretxt.requestFocus();
    }
    private void refrescar() {
        vista.dlmMercercia.clear();
        for (Merceria unPijama: modelo.obtenerMerceria()){
            vista.dlmMercercia.addElement(unPijama);
        }
    }

    /**
     * Metodo que añade los controles a los botones
     * @param listener
     */

    private void addActionListener(ActionListener listener) {
        vista.PIJAMASRadioButton.addActionListener(listener);
        vista.ROPAINTERIORRadioButton.addActionListener(listener);
        vista.EXPORTARButton.addActionListener(listener);
        vista.IMPORTARButton.addActionListener(listener);
        vista.NUEVOButton.addActionListener(listener);
        vista.ELIMINARButton.addActionListener(listener);
        vista.BUSCARButton.addActionListener(listener);
    }

    /**
     * Metedo que llama a la ventanda
     * @param listener
     */

    private void addWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    /**
     * Metodo que añade la lista al listener
     * @param listener
     */

    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }


    private void cargarDatosConfiguracion() throws IOException{
        Properties configuracion= new Properties();
        configuracion.load(new FileReader("merceria.conf"));
        ultimaRutaExportada= new File(configuracion.getProperty("ultimaRutaExportada"));
    }


    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada= ultimaRutaExportada;
    }

    private void guardarConfiguracion() throws IOException{
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("merceria.conf"), "Datos de la merceria");
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()){
            Merceria merceriaSeleccionada= (Merceria) vista.list1.getSelectedValue();
            vista.nombretxt.setText((merceriaSeleccionada.getNombre()));
            vista.marcatxt.setText((merceriaSeleccionada.getMarca()));
            vista.tipotxt.setText((merceriaSeleccionada.getTipo()));
            vista.fechaRegistro.setDate(merceriaSeleccionada.getFechaRegistro());

            if (merceriaSeleccionada instanceof Pijamas){
                vista.PIJAMASRadioButton.doClick();
                vista.telatxt.setText(String.valueOf(((Pijamas)merceriaSeleccionada).getTalla()));
            }else {
                vista.ROPAINTERIORRadioButton.doClick();
                vista.telatxt.setText(String.valueOf(((RopaInterior)merceriaSeleccionada).getPrecio()));
            }
        }
    }

    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }
    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }



    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }
}

