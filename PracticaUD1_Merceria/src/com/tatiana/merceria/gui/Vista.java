package com.tatiana.merceria.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.tatiana.merceria.base.Merceria;


import javax.swing.*;

public class Vista {
    public  JFrame frame;
    private  JPanel panel1;
    public JRadioButton PIJAMASRadioButton;
    public JRadioButton ROPAINTERIORRadioButton;
    public JTextField nombretxt;
    public JTextField marcatxt;
    public JTextField tipotxt;
    public JTextField telatxt;
    public JLabel Nombre;
    public JLabel Marca;
    public JLabel Tipo;
    public JLabel precioTalla;
    public JButton NUEVOButton;
    public JButton EXPORTARButton;
    public JButton IMPORTARButton;
    public DatePicker fechaRegistro;
    public JList list1;
    public  JLabel merceriaLabel;
    public JButton ELIMINARButton;
    public JTextField BUSCARtxt;
    public JButton BUSCARButton;

    public DefaultListModel<Merceria>dlmMercercia;

    public Vista(){
        frame= new JFrame("Merceria");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }
    private void initComponents(){
        dlmMercercia= new DefaultListModel<Merceria>();
        list1.setModel(dlmMercercia);
    }
}
